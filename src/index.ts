import * as readline from 'readline';
import commandController from './controller/command_controller';
import ParkingLot from './models/parking_lot';
import ParkingServiceImpl from './services/parking_service';

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: 'PARKLOT> '
  });
let service = new ParkingServiceImpl();
let parkingLot = new ParkingLot(service);
rl.prompt();
rl.on('line', (line:string) => {
    console.log(commandController(line, rl, parkingLot));
    rl.prompt();
}).on('close', () => {
    console.log('Have a great day! Thank you for playing with us.');
    process.exit(0);
});