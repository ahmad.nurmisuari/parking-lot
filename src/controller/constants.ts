export const COMMAND_CREATE_PARKING_LOT = 'create_parking_lot';
export const COMMAND_PARK = 'park';
export const COMMAND_LEAVE = 'leave';
export const COMMAND_STATUS = 'status';
export const COMMAND_EXIT = 'exit';
export const INVALID_COMMAND = 'Invalid command!';