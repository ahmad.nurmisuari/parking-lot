import Car from "../models/car";
import ParkingLot from "../models/parking_lot";
import { COMMAND_CREATE_PARKING_LOT, COMMAND_EXIT, COMMAND_LEAVE, COMMAND_PARK, COMMAND_STATUS, INVALID_COMMAND } from "./constants";

export default function commandController(line:string, readline:any, parkingLot:ParkingLot){
    const data = line.trim().split(" ");
    const car: Car = new Car(data[1]);
        switch(data[0].toLowerCase()) {
                  case COMMAND_CREATE_PARKING_LOT:
                    return parkingLot.createParkingSpace(parseInt(data[1]));
                  case COMMAND_PARK:
                    return parkingLot.park(car);
                  case COMMAND_LEAVE:
                    return parkingLot.leave(car, parseInt(data[2]));
                  case COMMAND_STATUS:
                    return parkingLot.status();
                   case COMMAND_EXIT:
                    readline.close();
                    return;
                  default:
                    return INVALID_COMMAND;
        }
}