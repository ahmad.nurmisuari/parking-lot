import ParkingLot from "../../models/parking_lot";
import ParkingServiceImpl from "../../services/parking_service";
import commandController from "../command_controller";

describe('command controller', () => {
    const readline = () => {};
    let parkingLot: ParkingLot;
    beforeEach(() => {
        const service = new ParkingServiceImpl();
        parkingLot = new ParkingLot(service);
    });
    test('should return created parking lot when command create call',() =>{
        //Given
        const expected = 'Created parking lot with 6 slots';
        const commandCreate = 'create_parking_lot 6';
        //When
        const result = commandController(commandCreate, readline, parkingLot);
        //Then
        expect(result).toEqual(expected);
    });

    test('should parking the car when command park called',() =>{
        //Given
        const expected = 'Allocated slot number: 1';
        const commandCreate = 'create_parking_lot 6';
        const command = 'park KA-01-HH-1234';
        //When
        // create parking lot
        commandController(commandCreate, readline, parkingLot)
        const result = commandController(command, readline, parkingLot);
        //Then
        expect(result).toEqual(expected);
    });

    test('should leave the car when command leave called',() =>{
        //Given
        const expected = 'Registration number KA-01-HH-1234 with Slot Number 1 is free with Charge 10';
        const commandCreate = 'create_parking_lot 6';
        const commandPark = 'park KA-01-HH-1234';
        const command = 'leave KA-01-HH-1234 1';
        //When
        // create parking lot
        commandController(commandCreate, readline, parkingLot)
        // park 
        commandController(commandPark, readline, parkingLot)
        const result = commandController(command, readline, parkingLot);
        //Then
        expect(result).toEqual(expected);
    });

    test('should return status parking lot when command status called',() =>{
        //Given
        const expected = 'Slot No. - Registration No.\n1  KA-01-HH-1234';
        const commandCreate = 'create_parking_lot 1';
        const commandPark = 'park KA-01-HH-1234';
        const command = 'status';
        //When
        // create parking lot
        commandController(commandCreate, readline, parkingLot)
        // park 
        commandController(commandPark, readline, parkingLot)
        const result = commandController(command, readline, parkingLot);
        //Then
        expect(result).toEqual(expected);
    });

    test('should exit the terminal when command exit called',() =>{
        //Given
        const commandCreate = 'create_parking_lot 1';
        const commandPark = 'park KA-01-HH-1234';
        const command = 'exit';
        const readlineMock = {
            close: () => {}
        }
        //When
        // create parking lot
        commandController(commandCreate, readline, parkingLot)
        // park 
        commandController(commandPark, readline, parkingLot)
        const result = commandController(command, readlineMock, parkingLot);
        //Then
        expect(result).toHaveReturned;
    });

    test('should return invalid command when command is not valid',() =>{
        //Given
        const expected = 'Invalid command!';
        const command = 'start';
        //When
        const result = commandController(command, readline, parkingLot);
        //Then
        expect(result).toEqual(expected);
    });
});