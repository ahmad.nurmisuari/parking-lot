import Car from "../../models/car";
import ParkSpace from "../../models/parking_space";
import ParkingServiceImpl from "../parking_service";

describe('parking_service', () => {
    let service: ParkingServiceImpl;
    beforeEach(() => {
        service = new ParkingServiceImpl();
    });

    test('should create space with 3 slot', () => {
    //Given
    const emptySpace = new ParkSpace(null);
    const expected = [emptySpace,emptySpace,emptySpace];    
    //When
    const result = service.createParkingSpace(3);
    //Then
    expect(result).toEqual(expected);
    })

    test('should find empty space in parking space', () => {
        //Given
        const expected = 0;
        const parkingSpace : ParkSpace[] = service.createParkingSpace(3);    
        //When
        const result = service.findEmptySpace(parkingSpace);
        //Then
        expect(result).toEqual(expected);
    })

    test('should find the car in parking space', () => {
        //Given
        const expected = 0;
        const bmw = new Car('KA-01-HH-1234');
        const carInSpace = new ParkSpace(bmw);
        const parkingSpace : ParkSpace[] = [carInSpace];

        //When
        const result = service.findCar(bmw, parkingSpace);
        //Then
        expect(result).toEqual(expected);
    })

    test('should not find the car in parking space', () => {
        //Given
        const expected = -1;
        const bmw = new Car('KA-01-HH-1234');
        const carInSpace = new ParkSpace(null);
        const parkingSpace : ParkSpace[] = [carInSpace];

        //When
        const result = service.findCar(bmw, parkingSpace);
        //Then
        expect(result).toEqual(expected);
    })

    test('should update the parking space when park the car', () => {
        //Given
        const bmw = new Car('KA-01-HH-1234');
        const carInSpace = new ParkSpace(bmw);
        const parkingSpace : ParkSpace[] = [];
        const expected = [carInSpace]
        //When
        const result = service.updateCarPosition(bmw, 1, parkingSpace);
        //Then
        expect(result).toEqual(expected);
    })

    test('should update the parking space when leave the car', () => {
        //Given
        const bmw = new Car('KA-01-HH-1234');
        const carInSpace = new ParkSpace(null);
        const parkingSpace : ParkSpace[] = [];
        const expected = [carInSpace]
        //When
        const result = service.updateCarPosition(null, 1, parkingSpace);
        //Then
        expect(result).toEqual(expected);
    })

    test('should create status parking', () => {
        //Given
        const expectedString = 'Slot No. - Registration No.\n1  KA-01-HH-1234\n2  ';
        const bmw = new Car('KA-01-HH-1234'); 
        const carInSpace = new ParkSpace(bmw);
        const emptySpace = new ParkSpace(null);
        const parkingSpace : ParkSpace[] = [carInSpace, emptySpace];
        //When
        const result = service.status(parkingSpace);
        //Then
        expect(result).toEqual(expectedString);
    })

    test('should create status parking when empty parking space', () => {
        //Given
        const expectedString = 'Slot No. - Registration No.';
        //When
        const result = service.status(null);
        //Then
        expect(result).toEqual(expectedString);
    })

    test('should return cost 20 when total hours 3', () => {
        //Given
        const expected = 20;
        const totalHour = 3;
        //When
        const result = service.calculateFee(totalHour);
        //Then
        expect(result).toEqual(expected);
    })

    test('should return cost 10 when total hours 2', () => {
        //Given
        const expected = 10;
        const totalHour = 2;
        //When
        const result = service.calculateFee(totalHour);
        //Then
        expect(result).toEqual(expected);
    })

});