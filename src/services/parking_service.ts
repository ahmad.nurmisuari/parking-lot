import Car from "../models/car";
import ParkSpace from "../models/parking_space";

interface ParkingService {
    createParkingSpace(size:number): any;
    findEmptySpace(parkingSpace: ParkSpace[]): any;
    findCar(car:Car, parkingSpace: ParkSpace[]): any;
    updateCarPosition(car: Car, index: number, parkingSpace: ParkSpace[]): any;
    status(parkingLot:ParkSpace[] | null): any;
    calculateFee(hour:number): any;
}

class ParkingServiceImpl implements ParkingService{
    emptySpace = new ParkSpace(null);
    createParkingSpace(size: number) {
        let parkingSpace = [];
        for(let i = 0; i < size; i++){
            parkingSpace.push(this.emptySpace);
        }
        return parkingSpace;
    }
    findEmptySpace(parkingSpace: ParkSpace[]) {
        return parkingSpace.indexOf(this.emptySpace);
    }
    findCar(car:Car, parkingSpace: ParkSpace[]){
       const data = parkingSpace.find(element => element.space?.licenseNumber === car.licenseNumber) as ParkSpace;
       return  parkingSpace.indexOf(data);
    }
    updateCarPosition(car: any, index: number, parkingSpace: ParkSpace[]) {
       const space = parkingSpace;
       let carInSpace = new ParkSpace(car);
       if(carInSpace.space?.licenseNumber == null){
           carInSpace = this.emptySpace;
       }
       space.splice(index, 1, carInSpace);
       return space;
    }
    status(parkingLot:ParkSpace[] | any) {
        let i = 1;
        let result = 'Slot No. - Registration No.';
        if(parkingLot){
         for(let parkingSpace of parkingLot){
            result+= `\n${i++}  ${parkingSpace.space?.licenseNumber??''}`;
         }
        }
        return result;
    }
    calculateFee(hour:number) {
        let cost = 10;
        if(hour > 2){
            return cost + (hour - 2) * 10;
        }
        return cost;
    }
}

export default ParkingServiceImpl;