import Car from "../car";

describe('car_model', () => {

        test('should return object car when create new car with license number', () => {
            //Given
            const licenseNumber :string = 'KA-01-HH-1234';
            const expectedCar : Car = new Car(licenseNumber);
            //When
            const bmw : Car = new Car(licenseNumber);
           
            //Then
            expect(bmw).toEqual(expectedCar);
        })


        test('should return not equal when create new car with different license number', () => {
            //Given
            const licenseNumber :string = 'KA-01-HH-1234';
            const expectedCar : Car = new Car('KA-01-HH-9999');
            //When
            const bmw : Car = new Car(licenseNumber);
           
            //Then
            expect(bmw).not.toEqual(expectedCar);
        })

});