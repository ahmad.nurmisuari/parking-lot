import ParkingServiceImpl from '../../services/parking_service';
import Car from '../car';
import ParkingLot from '../parking_lot'

describe('parking_lot_model', () => {
    let parkingLot: ParkingLot;
    beforeEach(() => {
        const service = new ParkingServiceImpl();
        parkingLot = new ParkingLot(service);
    });

    // create parking lot
    test('should return parking lot with 6 slots when create parking lot with 6 slot', () => {
        //Given
        const expectedSlots = 6;
        const expectedString = 'Created parking lot with 6 slots';
        
        //When
        const result = parkingLot.createParkingSpace(6);
       
        //Then
        expect(parkingLot.size).toEqual(expectedSlots);
        expect(result).toEqual(expectedString);
    })

    //park
    test('should return allocated parking with slot number 1 when park the car with license number', () => {
        //Given
        const expectedString = 'Allocated slot number: 1';
        const licenseNumber = new Car('KA-01-HH-1234');
        
        //When
        parkingLot.createParkingSpace(6);
        const result = parkingLot.park(licenseNumber);
       
        //Then
        expect(result).toEqual(expectedString);
    })

    test('should return parking lot is full when park the car', () => {
        //Given
        const expectedString = 'Sorry, parking lot is full';
        const bmw = new Car('KA-01-HH-1234');
        const porsche = new Car('KA-01-HH-9999');

        //When
        parkingLot.createParkingSpace(1);
        parkingLot.park(bmw);
        const result = parkingLot.park(porsche);
       
        //Then
        expect(result).toEqual(expectedString);
    })


    test('should return the car already parked when park the car with duplicate license number', () => {
        //Given
        const expectedString = 'Registration number KA-01-HH-1234 already parked';
        const bmw = new Car('KA-01-HH-1234');
        const porsche = new Car('KA-01-HH-9999');
        
        //When
        parkingLot.createParkingSpace(3);
        parkingLot.park(bmw);
        parkingLot.park(porsche);
        const result = parkingLot.park(bmw);
       
        //Then
        expect(result).toEqual(expectedString);
    })

    //leave
    test('should return the car leave with total charge $10 when the car is leaving with 1 hours', () => {
        //Given
        const expectedString = 'Registration number KA-01-HH-1234 with Slot Number 1 is free with Charge 10';
        const bmw = new Car('KA-01-HH-1234');
        const porsche = new Car('KA-01-HH-9999');
        
        //When
        parkingLot.createParkingSpace(3);
        parkingLot.park(bmw);
        parkingLot.park(porsche);
        const result = parkingLot.leave(bmw, 1);
       
        //Then
        expect(result).toEqual(expectedString);
    })

    test('should return the car leave with total charge $30 when the car is leaving with 4 hours', () => {
        //Given
        const expectedString = 'Registration number KA-01-HH-1234 with Slot Number 1 is free with Charge 30';
        const bmw = new Car('KA-01-HH-1234');
        const porsche = new Car('KA-01-HH-9999');
        
        //When
        parkingLot.createParkingSpace(3);
        parkingLot.park(bmw);
        parkingLot.park(porsche);
        const result = parkingLot.leave(bmw, 4);
       
        //Then
        expect(result).toEqual(expectedString);
    })

    test('should return the car is not found when not exist car leaving', () => {
        //Given
        const expectedString = 'Registration number KA-01-HH-9999 not found';
        const bmw = new Car('KA-01-HH-1234');
        const porsche = new Car('KA-01-HH-9999');
        
        //When
        parkingLot.createParkingSpace(3);
        parkingLot.park(bmw);
        const result = parkingLot.leave(porsche, 3);
       
        //Then
        expect(result).toEqual(expectedString);
    })

    //status
    test('should return status parking lot', () => {
        //Given
        const expectedString = 'Slot No. - Registration No.\n1  KA-01-HH-1234';
        const bmw = new Car('KA-01-HH-1234');
        
        //When
        parkingLot.createParkingSpace(1);
        parkingLot.park(bmw);
        const result = parkingLot.status();
       
        //Then
        expect(result).toEqual(expectedString);
    })
});