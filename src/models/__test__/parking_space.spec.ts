import Car from "../car";
import ParkSpace from "../parking_space";

describe('parking space model', () => {

        test('should return parking space with object car when create new car with license number', () => {
            //Given
            const licenseNumber :string = 'KA-01-HH-1234';
            const expectedCar : Car = new Car(licenseNumber);
            const expectedSpace : ParkSpace = new ParkSpace(expectedCar);
            //When
            const newSpace : ParkSpace = new ParkSpace(expectedCar);
           
            //Then
            expect(newSpace.space).toEqual(expectedSpace.space);
        })


        test('should return  parking space with null object when create empty space', () => {
            //Given
            const value = null;
            //When
            const newSpace : ParkSpace = new ParkSpace(value);
           
            //Then
            expect(newSpace.space).toEqual(value);
        })

});