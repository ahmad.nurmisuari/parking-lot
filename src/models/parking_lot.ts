import ParkingService from "../services/parking_service";
import Car from "./car";
interface ParkingLot {
    createParkingSpace(size:number): any;
    park(car:Car): any;
    leave(car:Car, hour:number): any;
    status(): any;
}

class ParkingLotModel implements ParkingLot{
    parkingSpace!: any[];
    size!:number;
    parkingService!: ParkingService;

    constructor(service: ParkingService){
        this.parkingService = service;
    }

    createParkingSpace(size:number){
        this.size = size;
        this.parkingSpace = this.parkingService.createParkingSpace(size);
        return `Created parking lot with ${size} slots`;
    };

    park(car:Car){
        let index = this.parkingService.findEmptySpace(this.parkingSpace);
        if(index < 0){
            return 'Sorry, parking lot is full';
        }
        let indexCar = this.parkingService.findCar(car, this.parkingSpace);
        if(indexCar >= 0){
            return `Registration number ${car.licenseNumber} already parked`;
        }
        const slot = index + 1;
        this.parkingSpace = this.parkingService.updateCarPosition(car, index, this.parkingSpace);
        return 'Allocated slot number: '+ slot;
    };

    leave(car:Car, hour:number){
        let index = this.parkingService.findCar(car, this.parkingSpace);
        if(index < 0){
            return `Registration number ${car.licenseNumber} not found`;
        }
        this.parkingSpace = this.parkingService.updateCarPosition(null, index, this.parkingSpace);
        const chargeFee = this.parkingService.calculateFee(hour);
        return `Registration number ${car.licenseNumber} with Slot Number ${index+1} is free with Charge ${chargeFee}`;
    };
    
    status(){
        return this.parkingService.status(this.parkingSpace);
    };
}

export default ParkingLotModel;